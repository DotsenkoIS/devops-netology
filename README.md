# Репозиторий для занятий в онлайн школе netology.ru

## Оглавление 
## DevOps и системное администрирование
### Введение в DevOps
- [01-intro-01](https://github.com/dotsenkois/devops-netology/blob/main/01-intro-01/README.md)
- [02-git-01-vcs](https://github.com/dotsenkois/devops-netology/blob/main/02-git-01-vcs/README.md)
- [02-git-02-base](https://github.com/dotsenkois/devops-netology/tree/main/02-git-02-base)
- [02-git-03-branching](https://github.com/dotsenkois/devops-netology/blob/main/02-git-03-branching/README.md)
- [02-git-04-tools](https://github.com/dotsenkois/devops-netology/blob/main/02-git-04-tools/README.md)

### Основы системного администрирования
- [03-sysadmin-01-terminal](https://github.com/dotsenkois/devops-netology/tree/main/03-sysadmin-01-terminal)
- [03-sysadmin-02-terminal](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-02-terminal/README.md)
- [03-sysadmin-03-os](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-03-os/README.md)
- [03-sysadmin-04-os](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-04-os/README.md)
- [03-sysadmin-05-fs](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-05-fs/README.md)
- [03-sysadmin-06-net](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-06-net/README.md)
- [03-sysadmin-07-net](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-07-net/README.md)
- [03-sysadmin-08-net](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-08-net/README.md)
- [03-sysadmin-09-security](https://github.com/dotsenkois/devops-netology/blob/main/03-sysadmin-09-security/README.md)

### Скриптовые языки и языки разметки: Python, Bash, YAML, JSON
- [04-script-01-bash](https://github.com/dotsenkois/devops-netology/blob/main/04-script-01-bash/README.md)
- [04-script-02-py](https://github.com/dotsenkois/devops-netology/blob/main/04-script-02-py/README.md)
- [04-script-03-yaml](https://github.com/dotsenkois/devops-netology/blob/main/04-script-03-yaml/README.md)

## Виртуализация, базы данных и Terraform

### Виртуализация
- [05-virt-01-basics](https://github.com/dotsenkois/devops-netology/blob/main/05-virt-01-basics/README.md)
- [05-virt-02-iaac](https://github.com/dotsenkois/devops-netology/blob/main/05-virt-02-iaac/README.md)
- [05-virt-03-docker](https://github.com/dotsenkois/devops-netology/blob/main/05-virt-03-docker/README.md)
- [05-virt-04-docker-compose](https://github.com/dotsenkois/devops-netology/blob/main/05-virt-04-docker-compose/README.md)
- [05-virt-05-docker-swarm](https://github.com/dotsenkois/devops-netology/blob/main/05-virt-05-docker-swarm/README.md)

### Администрирование баз данных
- [06-db-01-basics](https://github.com/dotsenkois/devops-netology/blob/main/06-db-01-basics/README.md)
- [06-db-02-sql](https://github.com/dotsenkois/devops-netology/blob/main/06-db-03-mysql/README.md)
- [06-db-03-mysql](https://github.com/dotsenkois/devops-netology/blob/main/06-db-04-postgresql/README.md)
- [06-db-04-postgresql]()
- [06-db-05-elasticsearch](https://github.com/dotsenkois/devops-netology/blob/main/06-db-05-elasticsearch/README.md)
- [06-db-06-troobleshooting](https://github.com/dotsenkois/devops-netology/blob/main/06-db-06-troobleshooting/README.md)

### Облачная инфраструктура. Terraform.
- [07-terraform-01-intro](https://github.com/dotsenkois/devops-netology/blob/main/07-terraform-01-intro/README.md)
- [07-terraform-02-syntax](https://github.com/dotsenkois/devops-netology/blob/main/07-terraform-02-syntax/README.md)
- [07-terraform-03-basic](https://github.com/dotsenkois/devops-netology/blob/main/07-terraform-03-basic/README.md)
- [07-terraform-04-teamwork](https://github.com/dotsenkois/devops-netology/blob/main/07-terraform-04-teamwork/README.md)
- [07-terraform-05-golang](https://github.com/dotsenkois/devops-netology/blob/main/07-terraform-05-golang/README.md)
- [07-terraform-06-providers](https://github.com/dotsenkois/devops-netology/blob/main/07-terraform-06-providers/README.md)

## CI, мониторинг и управление конфигурациями.
### Система управления конфигурациями
- [08-ansible-01-base](https://github.com/dotsenkois/devops-netology/blob/main/08-ansible-01-base/README.md)
- [08-ansible-02-playbook](https://github.com/dotsenkois/devops-netology/blob/main/08-ansible-02-playbook/README.md)
- [08-ansible-03-yandex](https://github.com/dotsenkois/devops-netology/blob/main/08-ansible-03-yandex/README.md)
### Непрерывная разработка и интеграция
### Мониторинг и логи